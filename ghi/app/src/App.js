import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturersList from './ManufacturersList';
import ManufacturerForm from './ManufacturerForm';
import ModelsList from './ModelsList';
import ModelForm from './ModelForm';
import AutomobilesList from './AutobmobilesList';
import AutomobileForm from './AutomobileForm';
import TechniciansList from './TechniciansList';
import TechnicianForm from './TechnicianForm';
import AppointmentsList from './AppointmentsList';
import AppointmentForm from './AppointmentForm';
import AppointmentHistoryList from './AppointmentHistoryList';
import SalesPersonForm from './SalesPersonForm';
import SalespersonList from './SalespersonList';
import CustomerForm from './CustomerForm';
import CustomerList from './CustomerList';
import SalesForm from './SalesForm';
import SalesList from './SalesList';
import SalespersonHistory from './SalespersonHistory';


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />

          <Route path="manufacturers">
            <Route index element={<ManufacturersList />} />
            <Route path="create" element={<ManufacturerForm />} />
          </Route>
          <Route path="models">
            <Route index element={<ModelsList />} />
            <Route path="create" element={<ModelForm />} />
          </Route>
          <Route path="automobiles">
            <Route index element={<AutomobilesList />} />
            <Route path="create" element={<AutomobileForm />} />
          </Route>

          <Route path="technicians">
            <Route index element={<TechniciansList />} />
            <Route path="create" element={<TechnicianForm />} />
          </Route>
          <Route path="appointments">
            <Route index element={<AppointmentsList />} />
            <Route path="create" element={<AppointmentForm />} />
            <Route path="history" element={<AppointmentHistoryList />} />
          </Route>

          <Route path='salespeople'>
            <Route index element={<SalespersonList />} />
            <Route path='create' element={<SalesPersonForm />} />
          </Route>
          <Route path='customers'>
            <Route index element={<CustomerList />} />
            <Route path='create' element={<CustomerForm />} />
          </Route>
          <Route path='sales'>
            <Route index element={<SalesList />} />
            <Route path='create' element={<SalesForm />} />
            <Route path='history' element={<SalespersonHistory />} />
          </Route>

        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
