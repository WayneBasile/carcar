import React, {useEffect, useState } from 'react';

function ModelForm() {

  const [manufacturer, setManufacturer] = useState('');
  const [name, setName] = useState('');
  const [pictureUrl, setPictureUrl] = useState('');
  const [manufacturers, setManufacturerss] = useState([]);
  const [hasSignedUp, setHasSignedUp] = useState(false);

  const fetchData = async () => {
    const url = 'http://localhost:8100/api/manufacturers/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setManufacturerss(data.manufacturers);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.name = name;
    data.manufacturer_id = manufacturer;
    data.picture_url = pictureUrl;

    const manufacturerUrl = 'http://localhost:8100/api/models/';
    const fetchOptions = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const manufacturerResponse = await fetch(manufacturerUrl, fetchOptions);
    if (manufacturerResponse.ok) {
      setManufacturer('');
      setName('');
      setPictureUrl('');
      setHasSignedUp(true);
    }
  }

  const handleChangeManufacturer = (event) => {
    const value = event.target.value;
    setManufacturer(value);
  }

  const handleChangeName = (event) => {
    const value = event.target.value;
    setName(value);
  }

  const handleChangePictureUrl = (event) => {
    const value = event.target.value;
    setPictureUrl(value);
  }

  let spinnerClasses = 'd-flex justify-content-center mb-3';
  let dropdownClasses = 'form-select d-none';
  if (manufacturers.length > 0) {
    spinnerClasses = 'd-flex justify-content-center mb-3 d-none';
    dropdownClasses = 'form-select border-success';
  }

  let messageClasses = 'alert alert-success d-none mb-0';
  let formClasses = '';
  if (hasSignedUp) {
    messageClasses = 'alert alert-success mb-0';
    formClasses = 'd-none';
  }

  return (
    <div className="my-5 container">
      <div className="row">
        <div className="col"></div>
        <div className="col-5">
          <div className="card shadow">
            <div className="card-body">

              <form className={formClasses} onSubmit={handleSubmit} id="create-appointment-form">
                <h1 className="card-title">Create a Vehicle Model</h1>

                <div className="col">
                    <label className="fw-bold" htmlFor="name">Model Name</label>
                    <div className="form-group mb-3">
                        <input onChange={handleChangeName} required placeholder="" type="text" id="name" name="name" className="form-control border-success" />
                    </div>
                </div>

                <div className="col">
                    <label className="fw-bold" htmlFor="pictureUrl">Picture URL</label>
                    <div className="form-group mb-3">
                        <input onChange={handleChangePictureUrl} required placeholder="" type="text" id="picture_url" name="picture_url" className="form-control border-success" />
                    </div>
                </div>

                <div className="mb-3">
                  <label className="fw-bold" htmlFor="manufacturer">Manufacturer</label>

                  <div className={spinnerClasses} id="loading-manufacturer-spinner">
                    <div className="spinner-grow text-success" role="status">
                      <span className="visually-hidden">Loading...</span>
                    </div>
                  </div>

                  <select onChange={handleChangeManufacturer} name="manufacturer" id="manufacturer" className={dropdownClasses} required>
                    <option value="">Choose a manufacturer...</option>
                    {manufacturers.map(manufacturer => {
                      return (
                        <option key={manufacturer.href} value={manufacturer.id}>{manufacturer.name}</option>
                      )
                    })}
                  </select>
                </div>

                <button className="btn btn-lg btn-success">Create</button>
              </form>
              <div className={messageClasses} id="success-message">
                Vehicle model created successfully! If you'd like to create another model <a href="/models/create" className="alert-link">click here.</a>
              </div>

            </div>
          </div>
        </div>
        <div className="col"></div>
      </div>
    </div>
  );
}

export default ModelForm;
