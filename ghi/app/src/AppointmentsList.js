import React, {useEffect, useState } from 'react';
import Button from 'react-bootstrap/Button';

function AppointmentsList() {

  const [appointments, setAppointments] = useState([]);

  async function loadAppointments() {
    const response = await fetch('http://localhost:8080/api/appointments/');
    if (response.ok) {
      const data = await response.json();
      setAppointments(data.appointments.filter((appointment) => appointment.status === "upcoming"));
    }
  }

  useEffect(() => {
    loadAppointments();
  }, []);

  async function handleFinishAppointment(href) {
    const appointmentUrl = `http://localhost:8080${href}finish/`;
    const fetchOptions = {
      method: 'put',
      headers: {
        'Content-Type': 'application/json',
      },
    }
    const appointmentResponse = await fetch(appointmentUrl, fetchOptions);
    if (appointmentResponse.ok) {
      setAppointments(appointments.filter((appointment) => appointment.href !== href))
    }
  };

  async function handleCancelAppointment(href) {
    const appointmentUrl = `http://localhost:8080${href}cancel/`;
    const fetchOptions = {
      method: 'put',
      headers: {
        'Content-Type': 'application/json',
      },
    }
    const appointmentResponse = await fetch(appointmentUrl, fetchOptions);
    if (appointmentResponse.ok) {
      setAppointments(appointments.filter((appointment) => appointment.href !== href))
    }
  };

  function handleUpdateAppointment() {

  };

    return (
      <div className="container my-4">
        <h1 className="display-5 fw-bold">Scheduled Appointments</h1>
        <table className="table table-striped">
          <thead>
            <tr className="bg-success">
              <th className="text-white text-center">VIN</th>
              <th className="text-white text-center">Is VIP?</th>
              <th className="text-white text-center">Customer</th>
              <th className="text-white text-center">Date</th>
              <th className="text-white text-center">Time</th>
              <th className="text-white text-center">Technician</th>
              <th className="text-white text-center">Reason</th>
              <th className="text-white text-center">Manage Appointments</th>
            </tr>
          </thead>
          <tbody>
            {appointments && appointments.map(appointment => {
              let vip_status = "No";
              if (appointment.vip) {vip_status = "Yes"};
              return (
                <tr key={appointment.href} value={appointment.href}>
                  <td className="table-success text-center fw-bold">{ appointment.vin }</td>
                  <td className="table-success text-center fw-bold">{ vip_status }</td>
                  <td className="table-success text-center fw-bold">{ appointment.customer }</td>
                  <td className="table-success text-center fw-bold">{ new Date(appointment.date_time).toLocaleDateString() }</td>
                  <td className="table-success text-center fw-bold">{ new Date(appointment.date_time).toLocaleTimeString([], {hour: '2-digit', minute:'2-digit'}) }</td>
                  <td className="table-success text-center fw-bold">{ appointment.technician.first_name } {appointment.technician.last_name}</td>
                  <td className="table-success text-center fw-bold">{ appointment.reason }</td>
                  <td className="table-success text-center fw-bold">
                  <div className="d-grid gap-4 d-flex mx-4 justify-content-md-center">
                    <Button className="btn w-50 btn-success fw-bold btn-sm d-none d-md-block text-light shadow-sm" onClick={() => handleFinishAppointment(appointment.href)} variant="outline-dark">Finsh</Button>
                    <Button className="btn w-50 btn-warning fw-bold btn-sm d-none d-md-block text-light shadow-sm" onClick={() => handleUpdateAppointment(appointment.href)} variant="outline-dark">Update</Button>
                    <Button className="btn w-50 btn-danger fw-bold btn-sm d-none d-md-block text-light shadow-sm" onClick={() => handleCancelAppointment(appointment.href)} variant="outline-dark">Cancel</Button>
                  </div>
                  </td>
                </tr>
              );
            })}
          </tbody>
      </table>
      </div>
    );
  }

  export default AppointmentsList;
