import React from 'react';
import Button from 'react-bootstrap/Button';

function MainPage() {
  return (
    <div className="my-5 container">
      <div className="row">
        <div className="col"></div>
        <div className="col-6 bg-success rounded">
        <img className="bg-white rounded d-block mx-auto mb-3 img-fluid" src="/logo.png" alt="" width="350"/>
        <p className="d-flex lead mb-4 fw-bold fst-italic justify-content-center text-light" style={{textShadow: "1px 0 0 #000, 0 -1px 0 #000, 0 1px 0 #000, -1px 0 0 #000"}}>
              The premiere solution for automobile dealership management!
        </p>
        <div className="d-grid gap-4 d-flex mb-4 mx-4">
              <Button className="btn w-50 btn-success btn-lg d-none d-md-block text-light shadow-sm" href="/appointments/create" variant="outline-dark">Schedule an Appointment</Button>
              <Button className="btn w-50 btn-success btn-lg d-none d-md-block text-light shadow-sm" href="/customers/create" variant="outline-dark">Create a Customer</Button>
        </div>
        </div>
        <div className="col"></div>
      </div>
    </div>
  );
}

export default MainPage;
