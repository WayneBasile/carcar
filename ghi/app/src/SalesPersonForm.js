import React, {useState} from 'react'

function SalesPersonForm() {

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [employeeId, setEmployeeId] = useState('');
    const [hasSignedUp, setHasSignedUp] = useState(false);


    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.first_name = firstName;
        data.last_name = lastName;
        data.employee_id = employeeId;

        const salesPersonUrl = 'http://localhost:8090/api/salespeople/';
        const fetchOptions = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const salesPersonResponse = await fetch(salesPersonUrl, fetchOptions);
        if (salesPersonResponse.ok) {
            setFirstName('');
            setLastName('');
            setEmployeeId('');
            setHasSignedUp(true);
        }
    }

    const handleFirstNameChange = (event) => {
        const value = event.target.value;
        setFirstName(value);
    }

    const handleLastNameChange = (event) => {
        const value = event.target.value;
        setLastName(value);
    }

    const handleEmployeeIdChange = (event) => {
        const value = event.target.value;
        setEmployeeId(value);
    }


    let messageClasses = 'alert alert-success d-none mb-0';
    let formClasses = '';
    if (hasSignedUp) {
        messageClasses = 'alert alert-success mb-0';
        formClasses = 'd-none';
    }

    return (
        <div className='my-5 container'>
            <div className='row'>
                <div className='col'></div>
                <div className='col'>
                    <div className='card shadow'>
                        <div className='card-body'>
                            <form className={formClasses} onSubmit={handleSubmit} id='create-salesperson-form'>
                                <h1 className='card-title'>Add a salesperson</h1>
                                    <div className='col'>
                                    <label className='fw-bold' htmlFor='first name'>First Name</label>
                                            <div className='form-floating mb-3'>
                                                <input onChange={handleFirstNameChange} required placeholder='First name' type='text' id='first_name' name='first_name' className='form-control' />
                                            </div>
                                    </div>

                                    <div className='col'>
                                        <label className='fw-bold' htmlFor='last name'>Last Name</label>
                                            <div className='form-floating mb-3'>
                                                <input onChange={handleLastNameChange} required placeholder='Last name' type='text' id='last_name' name='last_name' className='form-control' />
                                            </div>
                                    </div>

                                    <div className='col'>
                                        <label className='fw-bold' htmlFor='employee id'>Employee ID</label>
                                            <div className='form-floating mb-3'>
                                                <input onChange={handleEmployeeIdChange} required placeholder='Employee ID' type='text' id='employee_id' name='employee_id' className='form-control' />
                                            </div>
                                    </div>

                                <button className='btn btn-lg btn-success'>Create Salesperson</button>
                            </form>
                            <div className={messageClasses} id='success-message'>
                                Salesperson created successfully! If you would like to create another salesperson <a href='/salepeople/create' className='alert-link'>click here. </a>
                                To see the salesperson list <a href='/salespeople' className='alert-link'>click here.</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='col'></div>
            </div>
        </div>
    );

}
export default SalesPersonForm;
