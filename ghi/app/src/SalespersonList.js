import { useEffect, useState } from "react";
import Button from "react-bootstrap/Button";

function SalespersonList() {

    const [salespeople, setSalespeople] = useState([]);

    async function loadSalespeople() {
        const response = await fetch('http://localhost:8090/api/salespeople/');
        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespeople);
        }
    }

    useEffect(() => {
        loadSalespeople();
    }, []);

    async function handleDeleteSalesperson(href) {
        const salespersonUrl = `http://localhost:8090${href}`;
        const fetchOptions = {
            method: "DELETE",
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const salesPersonResponse = await fetch(salespersonUrl, fetchOptions);
        if (salesPersonResponse.ok) {
            loadSalespeople();
        }
    };

    return(
        <div className='container my-4'>
            <h1 className='display-5 fw-bold'>Salespeople</h1>
            <table className='table table-striped'>
                <thead>
                    <tr className='bg-success'>
                        <th className='text-white text-center'>First Name</th>
                        <th className='text-white text-center'>Last Name</th>
                        <th className='text-white text-center'>Employee ID</th>
                        <th className='text-white text-center'>Remove Salesperson</th>
                    </tr>
                </thead>
                <tbody>
                    {salespeople && salespeople.map(salesperson => {
                        return (
                            <tr key={salesperson.href} value={salesperson.href}>
                                <td className='table-success text-center fw-bold'>{salesperson.first_name}</td>
                                <td className='table-success text-center fw-bold'>{salesperson.last_name}</td>
                                <td className='table-success text-center fw-bold'>{salesperson.employee_id}</td>
                                <td className='table-success text-center fw-bold'>
                                <div className='d-grid gap-4 d-flex mx-4 justify-content-md-center'>
                                    <Button className="btn w-50 btn-danger fw-bold btn-sm d-none d-md-block text-light shadow-sm" onClick={() => handleDeleteSalesperson(salesperson.href)} variant="outline-dark">Delete</Button>
                                    </div>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default SalespersonList;
