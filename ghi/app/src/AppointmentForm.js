import React, {useEffect, useState } from 'react';

function AppointmentForm() {

  const [technician, setTechnician] = useState('');
  const [vin, setVin] = useState('');
  const [date, setDate] = useState('');
  const [time, setTime] = useState('');
  const [customer, setCustomer] = useState('');
  const [technicians, setTechnicians] = useState([]);
  const [reason, setReason] = useState('');
  const [hasSignedUp, setHasSignedUp] = useState(false);

  const fetchData = async () => {
    const url = 'http://localhost:8080/api/technicians/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.vin = vin;
    data.technician = technician;
    data.customer = customer;
    data.reason = reason;
    data.date_time = `${date} ${time}`;

    const technicianUrl = 'http://localhost:8080/api/appointments/';
    const fetchOptions = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const technicianResponse = await fetch(technicianUrl, fetchOptions);
    if (technicianResponse.ok) {
      setTechnician('');
      setVin('');
      setCustomer('');
      setDate('');
      setTime('');
      setReason('');
      setHasSignedUp(true);
    }
  }

  const handleChangeTechnician = (event) => {
    const value = event.target.value;
    setTechnician(value);
  }

  const handleChangeVin = (event) => {
    const value = event.target.value;
    setVin(value);
  }

  const handleChangeCustomer = (event) => {
    const value = event.target.value;
    setCustomer(value);
  }

  const handleChangeDate = (event) => {
    const value = event.target.value;
    setDate(value);
  }

  const handleChangeTime = (event) => {
    const value = event.target.value;
    setTime(value);
  }

  const handleChangeReason = (event) => {
    const value = event.target.value;
    setReason(value);
  }

  let spinnerClasses = 'd-flex justify-content-center mb-3';
  let dropdownClasses = 'form-select d-none';
  if (technicians.length > 0) {
    spinnerClasses = 'd-flex justify-content-center mb-3 d-none';
    dropdownClasses = 'form-select border-success';
  }

  let messageClasses = 'alert alert-success d-none mb-0';
  let formClasses = '';
  if (hasSignedUp) {
    messageClasses = 'alert alert-success mb-0';
    formClasses = 'd-none';
  }

  return (
    <div className="my-5 container">
      <div className="row">
        <div className="col"></div>
        <div className="col">
          <div className="card shadow">
            <div className="card-body">

              <form className={formClasses} onSubmit={handleSubmit} id="create-appointment-form">
                <h1 className="card-title">Create a Service Appointment</h1>

                <div className="col">
                    <label className="fw-bold" htmlFor="vin">Automobile Vin</label>
                    <div className="form-group mb-3">
                        <input onChange={handleChangeVin} required placeholder="" type="text" id="vin" name="vin" className="form-control border-success" />
                    </div>
                </div>

                <div className="col">
                    <label className="fw-bold" htmlFor="customer">Customer</label>
                    <div className="form-group mb-3">
                        <input onChange={handleChangeCustomer} required placeholder="" type="text" id="customer" name="customer" className="form-control border-success" />
                    </div>
                </div>

                <div className="col">
                    <label className="fw-bold" htmlFor="vin">Date</label>
                    <div className="form-group mb-3">
                        <input onChange={handleChangeDate} required placeholder="" type="date" id="date" name="date" className="form-control border-success" />
                    </div>
                </div>


                <div className="col">
                    <label className="fw-bold" htmlFor="time">Time</label>
                    <div className="form-group mb-3">
                        <input onChange={handleChangeTime} required placeholder="" type="time" id="time" name="time" className="form-control border-success" />
                    </div>
                </div>

                <div className="mb-3">
                  <label className="fw-bold" htmlFor="technician">Technician</label>

                  <div className={spinnerClasses} id="loading-technician-spinner">
                    <div className="spinner-grow text-success" role="status">
                      <span className="visually-hidden">Loading...</span>
                    </div>
                  </div>

                  <select onChange={handleChangeTechnician} name="technician" id="technician" className={dropdownClasses} required>
                    <option value="">Choose a technician...</option>
                    {technicians.map(technician => {
                      return (
                        <option key={technician.href} value={technician.employee_id}>{technician.first_name} {technician.last_name}</option>
                      )
                    })}
                  </select>
                </div>

                <div className="col">
                    <label className="fw-bold" htmlFor="reason">Reason</label>
                    <div className="form-group mb-3">
                        <input onChange={handleChangeReason} required placeholder="" type="text" id="reason" name="reason" className="form-control border-success" />
                    </div>
                </div>

                <button className="btn btn-lg btn-success">Create</button>
              </form>
              <div className={messageClasses} id="success-message">
                Appointment created succesfully! If you'd like to create another appointment <a href="/appointments/create" className="alert-link">click here.</a>
              </div>

            </div>
          </div>
        </div>
        <div className="col"></div>
      </div>
    </div>
  );
}

export default AppointmentForm;
