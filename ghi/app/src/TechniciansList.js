import React, {useEffect, useState } from 'react';
import Button from 'react-bootstrap/Button';

function TechniciansList() {

  const [technicians, setTechnicians] = useState([]);
  const [selected, setSelected] = useState("");

  async function loadTechnicians() {
    const response = await fetch('http://localhost:8080/api/technicians/');
    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians);
    }
  }

  useEffect(() => {
    loadTechnicians();
  }, []);

  async function handleDeleteTechnician(href) {
    setBadReq(false);
    const technicianUrl = `http://localhost:8080${href}`;
    const fetchOptions = {
      method: 'delete',
      headers: {
        'Content-Type': 'application/json',
      },
    }
    const technicianResponse = await fetch(technicianUrl, fetchOptions);
    if (technicianResponse.ok) {
      setTechnicians(technicians.filter((technician) => technician.href !== href))
    }
  };

  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [employeeId, setEmployeeId] = useState('');
  const [badReq, setBadReq] = useState(false);
  const [message, setMessage] = useState("");

  let badReqClasses = "alert alert-danger d-none";
  if (badReq) {
    badReqClasses = "alert alert-danger";
  }

  async function handleUpdateTechnician(href) {
    setBadReq(false);
    href === selected ? setSelected(""): setSelected(href)
  };

  async function handleSendRequest(href) {
    setBadReq(false);
    const data = {};
    data.first_name = firstName;
    data.last_name = lastName;
    data.employee_id = employeeId;

    const technicianUrl = `http://localhost:8080${href}`;
    const fetchOptions = {
      method: 'put',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const technicianResponse = await fetch(technicianUrl, fetchOptions);
    if (technicianResponse.ok) {
      setSelected("");
      loadTechnicians();
    }  else {
      const json = await technicianResponse.json();
      setMessage(json.message);
      setBadReq(true);
    }
  };

  const handleChangeFirstName = (event) => {
    const value = event.target.value;
    setFirstName(value);
  }

  const handleChangeLastName = (event) => {
    const value = event.target.value;
    setLastName(value);
  }

  const handleChangeEmployeeId = (event) => {
    const value = event.target.value;
    setEmployeeId(value);
  }

    return (
      <div className="container my-4">
        <h1 className="display-5 fw-bold">Technicians</h1>
        <div className={badReqClasses} id="fail-message">{message}</div>
        <table className="table table-striped">
          <thead>
            <tr className="bg-success">
              <th className="text-white text-center">Employee ID</th>
              <th className="text-white text-center">First Name</th>
              <th className="text-white text-center">Last Name</th>
              <th className="text-white text-center">Manage Technicians</th>
            </tr>
          </thead>
          <tbody>
            {technicians && technicians.map(technician => {
              return (
                <tr key={technician.href} value={technician.href}>
                  <td className="table-success text-center fw-bold w-25">
                    { selected === technician.href ?
                    <div className="form-group">
                      <input onChange={handleChangeEmployeeId} type="text" id="first_name" name="first_name" className="form-control border-success text-center" placeholder={ technician.employee_id }/>
                    </div>
                    :
                    <div>
                      { technician.employee_id }
                    </div>
                    }
                  </td>

                  <td className="table-success text-center fw-bold w-25">
                  { selected === technician.href ?
                    <div className="form-group">
                      <input onChange={handleChangeFirstName} type="text" id="first_name" name="first_name" className="form-control border-success text-center" placeholder={ technician.first_name }/>
                    </div>
                    :
                    <div>
                      { technician.first_name }
                    </div>
                    }
                  </td>

                  <td className="table-success text-center fw-bold w-25">
                  { selected === technician.href ?
                    <div className="form-group">
                      <input onChange={handleChangeLastName} type="text" id="first_name" name="first_name" className="form-control border-success text-center" placeholder={ technician.last_name } />
                    </div>
                    :
                    <div>
                      { technician.last_name }
                    </div>
                    }
                  </td>
                  <td className="table-success text-center fw-bold w-25">

                  <div className="d-grid gap-4 d-flex mx-4 justify-content-md-center">
                    <Button className="btn w-50 btn-warning fw-bold btn-sm d-none d-md-block text-light shadow-sm" onClick={() => handleUpdateTechnician(technician.href)} variant="outline-dark">{technician.href === selected ? "Cancel" : "Update"}</Button>

                    {technician.href === selected ?
                    <Button className="btn w-50 btn-success fw-bold btn-sm d-none d-md-block text-light shadow-sm" onClick={() => handleSendRequest(technician.href)} variant="outline-dark">Submit</Button>
                    :
                    <Button className="btn w-50 btn-danger fw-bold btn-sm d-none d-md-block text-light shadow-sm" onClick={() => handleDeleteTechnician(technician.href)} variant="outline-dark">Delete</Button>
                    }

                  </div>

                  </td>
                </tr>
              );
            })}
          </tbody>
      </table>
      </div>
    );
  }

  export default TechniciansList;
